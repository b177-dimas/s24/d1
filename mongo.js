// Query Operators

// [SECTION] Comparison Query Operators

// $gt and $gte operator (Greater than/Greater than or equal to)

//find users with an age greater than 50
db.users.find({
	age: {
		$gt: 50
	}
})

//find users with an age greater than or equal to 50
db.users.find({
	age: {
		$gte: 50
	}
})

//find users with an age less than 50
db.users.find({
	age: {
		$lt: 50
	}
})

//find users with an age less than or equal to 50
db.users.find({
	age: {
		$lte: 50
	}
})

//find users with an age that is NOT equal to 82
db.users.find({
	age: {
		$ne: 82
	}
})

//find users whose last names are either "Hawking" or "Doe"
db.users.find({
	lastName: {
		$in: ["Hawking", "Doe"]
	}
})

// db.users.find({
// 	lastName: "Hawking"
// })

//find users whose courses including "HTML" OR "React"
db.users.find({
	courses: {
		$in: ["HTML", "React"]
	}
})

// [Section] Logical Query Operators

//$or operator
db.users.find({
	$or: [
		{
			firstName: "Neil"
		},
		{
			age: 21
		}
	]
})

db.users.find({
	$or: [
		{
			lastName: "Hawking"
		},
		{
			lastName: "Doe"
		}
	]
})

//$and operator
db.users.find({
	$and: [
		{
			age: {
				$ne: 82
			}
		},
		{
			age: {
				$ne: 76
			}
		}
	]
})
